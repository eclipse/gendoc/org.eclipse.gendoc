/*****************************************************************************
 * Copyright (c) 2010 Atos Origin.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Alexia Allanic (Atos Origin) alexia.allanic@atosorigin.com - Initial API and implementation
 *  Papa Malick Wade (Atos Origin) papa-malick.wade@atosorigin.com - extension the format of diagrams 
 *****************************************************************************/
package org.eclipse.gendoc.bundle.acceleo.gmf.impl;

import java.io.File;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain.Factory;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gendoc.bundle.acceleo.gmf.service.GMFDiagramRenderer;
import org.eclipse.gendoc.bundle.acceleo.gmf.service.IDiagramRenderer;
import org.eclipse.gendoc.bundle.acceleo.gmf.service.IDiagramRenderer.FileFormat;
import org.eclipse.gendoc.documents.FileRunnable;
import org.eclipse.gendoc.documents.IImageManipulationService;
import org.eclipse.gendoc.documents.IImageManipulationServiceFactory;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.IGendocDiagnostician;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.widgets.Display;

public class GMFDiagramRunnable implements FileRunnable {

	private Diagram diagram;

	/** The extension of diagram. */
	private FileFormat extension;
	private final List<EObject> visibleElements;

	public GMFDiagramRunnable(Diagram diagram, String extension) {
		this(diagram, extension, null);
	}

	public GMFDiagramRunnable(Diagram diagram, String extension,
			List<EObject> visibleElements) {
		this.diagram = diagram;
		this.extension = FileFormat.transformToFormat(extension);
		if (visibleElements != null) {
			this.visibleElements = visibleElements;
		} else {
			this.visibleElements = Collections.emptyList();
		}

	}

	/**
	 * Instantiates a new diagram runnable.
	 * 
	 * @param diagram
	 *            the diagram
	 */
	public void run(final String resourceId, final String outputResourceFolder) {
		if (Realm.getDefault() == null) {
			Realm.runWithDefault(SWTObservables.getRealm(Display.getDefault()), new Runnable() {
				public void run() {
					doRun(resourceId, outputResourceFolder);
				}
			});
		}
		else {
			doRun(resourceId, outputResourceFolder);
		}
		
	}
	
	protected void doRun(String resourceId, String outputResourceFolder) {
		if (extension != null) {
			IDiagramRenderer diagramRenderer = GendocServices.getDefault().getService(IDiagramRenderer.class);
			if (diagramRenderer == null)
				diagramRenderer = new GMFDiagramRenderer();
			
			new File(outputResourceFolder).mkdirs();
			IPath path = new Path(outputResourceFolder + "/" + resourceId
					+ extension.getFullExtension());
			try {
				// this part of code is necessary for Transactional Editing
				// Domain by GMF
				// if this section is removed a NPE occurs in copyToImage
				// Function (no Editing Domain)
				{
					Resource eResource = diagram.eResource();
					if (eResource != null) {
						ResourceSet resourceSet = eResource.getResourceSet();
						if (TransactionUtil.getEditingDomain(resourceSet) == null) {
							Factory factory = TransactionalEditingDomain.Factory.INSTANCE;
							factory.createEditingDomain(resourceSet);
						}
					}
				}
				
				diagramRenderer.renderDiagram(diagram, visibleElements, path, extension, new NullProgressMonitor());

				IImageManipulationServiceFactory imageManipulationServiceFactory = GendocServices.getDefault().getService(IImageManipulationServiceFactory.class);
	        	IImageManipulationService imageManipulationService = imageManipulationServiceFactory.getService(extension.name().toLowerCase()) ;
	        	imageManipulationService.transform(path);
				
			} catch (CoreException e) {
				IGendocDiagnostician diag = GendocServices.getDefault().getService(IGendocDiagnostician.class);
				if (diag != null)
				{
					diag.addDiagnostic(IStatus.WARNING,"no image can be generated for Diagram : " + diagram.toString(), new Object[]{diagram} );
				}
				e.printStackTrace();
			} catch (Exception ex) {
				IGendocDiagnostician diag = GendocServices.getDefault().getService(IGendocDiagnostician.class);
				if (diag != null)
				{
					diag.addDiagnostic(IStatus.WARNING,"no image can be generated for Diagram : " + diagram.toString(), new Object[]{diagram} );
				}
				ex.printStackTrace();
			}
		}

	}

	public String getFileExtension() {
		return extension.toString().toLowerCase();
	}

}
