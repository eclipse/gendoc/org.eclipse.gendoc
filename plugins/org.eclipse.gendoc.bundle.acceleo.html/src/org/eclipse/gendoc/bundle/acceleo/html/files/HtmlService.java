/*****************************************************************************
 * Copyright (c) 2018 Ericsson AB.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.bundle.acceleo.html.files;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.w3c.tidy.Tidy;

public class HtmlService {
	private static XPath XPATH = XPathFactory.newInstance().newXPath();
	private static NullOutputStream NULL_OUTPUT_STREAM = new NullOutputStream();
	
	public static boolean isHtml(String input) {
		return isHtml(parseHtml(input));
	}
	
	public static String stripHtmlTags(String input) {
		Document doc = parseHtml(input);
		if (!isHtml(doc))
			return input;
		
		HtmlStripTagVisitor visitor = new HtmlStripTagVisitor();
		HtmlUtils.visit(getBodyElement(doc), visitor);
		return visitor.toString();
	}
	
	public static String htmlToText(String input) {
		Document doc = parseHtml(input);
		if (!isHtml(doc))
			return input;
		
		HtmlToTextVisitor visitor = new HtmlToTextVisitor();
		HtmlUtils.visit(getBodyElement(doc), visitor);
		return visitor.toString();
	}

	public static String textToHtml(String input) {
		Document doc = parseHtml(input);
		if (isHtml(doc))
			return input;
		
		TextToHtmlBuilder htmlBuilder = new TextToHtmlBuilder();
		htmlBuilder.addText(input);
		return htmlBuilder.toString();
		
	}
		
	private static class NullOutputStream extends OutputStream {
		@Override
		public void write(int b) throws IOException {}
	}

	private static Tidy initParser() {
		Tidy t = new Tidy();
		Properties oProps = new Properties();
		oProps.setProperty("new-blocklevel-tags", "hhh");
		t.setMakeClean(true);
		t.setXmlOut(true);
		t.setQuiet(true);
		t.setRawOut(true);
		t.setOnlyErrors(false);
		t.setShowWarnings(false);
		return t;
	}
	
	private static Element getBodyElement(Document doc) {
		try {
			return (Element)XPATH.evaluate("/html/body", doc, XPathConstants.NODE);	
		} catch (XPathExpressionException e) {
			return null;
		}
	}
	
	public static Document parseHtml(String input) {
		Tidy t = initParser();
		Document doc = t.parseDOM(new ByteArrayInputStream(input.getBytes()), NULL_OUTPUT_STREAM);
		return doc;
	}

	private static boolean isHtml(Document doc) {
		Element root = getBodyElement(doc);
		if (root == null)
			return false;
		NodeList nl = root.getChildNodes();
		for (int i=0; i<nl.getLength(); i++) {
			if (!(nl.item(i) instanceof Text))
				return true;
		}
			
		return false;
	}
	
	public static void main(String[] args) {
		String txt = "The operational support functionality of the digital business systems is a common platform that can support the operatrional support needs of the network and operational domains for a service provider.\n"+
					 "A base set of functions with base interfaces is used to build the main functions of the OSS, but not all.\n\n"+
					 "a) *Service and Resource Life Cycle Management Function*";
		System.out.println(textToHtml(txt));
	}
}
