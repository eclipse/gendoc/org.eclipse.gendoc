package org.eclipse.gendoc.bundle.acceleo.sirius.service;

import org.eclipse.gendoc.bundle.acceleo.gmf.service.IDiagramRenderer;
import org.eclipse.gendoc.process.AbstractProcess;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.exception.GenDocException;

public class SiriusDiagramRendererOverriding extends AbstractProcess {

	@Override
	protected void doRun() throws GenDocException {
        SiriusDiagramRenderer diagramRenderer = new SiriusDiagramRenderer();
        diagramRenderer.setServiceId("DiagramRenderer");//$NON-NLS-1$
        GendocServices.getDefault().setService(IDiagramRenderer.class, diagramRenderer);
	}

	@Override
	protected int getTotalWork() {
		return 1;
	}

}
