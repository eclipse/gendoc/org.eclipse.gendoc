/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.document.parser.documents.openoffice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPathExpressionException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gendoc.document.parser.Activator;
import org.eclipse.gendoc.document.parser.documents.AbstractZipDocument;
import org.eclipse.gendoc.document.parser.documents.XMLParser;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class OpenOfficeDocument extends AbstractZipDocument {
	private HashMap<String, XMLParser> subdocuments;

	public OpenOfficeDocument(File documentFile, Map<CONFIGURATION, Boolean> configuration) throws IOException {
		super(documentFile, configuration);
	}

	public OpenOfficeDocument(File document) throws IOException {
		super(document);
	}

	public OpenOfficeDocument(URL documentFile, Map<CONFIGURATION, Boolean> configuration) {
		super(documentFile, configuration);
	}

	public OpenOfficeDocument(URL document) {
		super(document);
	}

	@Override
	public OpenOfficeParser getXMLParser() {
		return (OpenOfficeParser)super.getXMLParser();
	}

	public String getNextDocumentName(String relpath) {
		File rel = new File(relpath.replace("/", File.separator));
		String[] nameParts = rel.getName().split("\\*");
		File f = new File(getUnzipLocationDocumentFile(),rel.getPath());
		File folder = f.getParentFile();
		int index = 0;
		if (folder.exists()) {
			String names[] = folder.list();			
			for (int i=0; names != null && i<names.length; i++) {
				if (names[i].startsWith(nameParts[0]) && names[i].endsWith(nameParts[1])) {
					try {
						index = Math.max(Integer.valueOf(
								names[i].replace(nameParts[0], "").replace(nameParts[1], "")),index);
					} catch (NumberFormatException e) {}					
				}				
			}
		}
		
		String res = rel.getParent()+File.separator+rel.getName().replace("*",String.valueOf(index+1));
		return res.replace(File.separator, "/");
	}

    public XMLParser createSubdocument(String path, CharSequence content) throws IOException {
    	return createSubdocument(path, content, null);
    }
    
    public XMLParser createSubdocument(String path, CharSequence content, String contentType) throws IOException {
		File f = new File(getUnzipLocationDocumentFile(),path.replace("/", File.separator));
		f.getParentFile().mkdirs();
    	FileOutputStream writer = new FileOutputStream(f);
    	writer.write(String.valueOf(content).getBytes(Charset.forName("UTF-8")));
    	writer.flush();
    	writer.close();
    	
    	if (contentType != null) {
	    	if (contentTypes == null)
	    		contentTypes = getSubdocument("/[Content_Types].xml");
	    	
	    	Node typesEl = null;
			try {
				typesEl = getXMLParser().getXPathUtils().evaluateNode(contentTypes.getDocument(), "/ct:Types");
				Node n = getXMLParser().getXPathUtils().evaluateNode(typesEl, "./ct:Override[@PartName='"+path+"']");
		    	if (n != null)
		    		n.getParentNode().removeChild(n);
		    	Element el = (Element)typesEl.appendChild(typesEl.getOwnerDocument().createElementNS(
		    			OpenOfficeHelper.CONTENT_TYPES_NAMESPACE, "ct:Override"));
		    	el.setAttribute("PartName", path);
		    	el.setAttribute("ContentType", contentType);
			} catch (XPathExpressionException e) {
				Activator.log(e,IStatus.ERROR);
			}
    	}	
    	subdocuments.remove(path);
		return getSubdocument(path);
    }

	@SuppressWarnings({"unchecked" })
	public List<XMLParser> getSubdocuments(String folder) {
		if (subdocuments == null)
			return Collections.EMPTY_LIST;
		
		List<XMLParser> parsers = new ArrayList<XMLParser>();
		for (Map.Entry<String,XMLParser> e : subdocuments.entrySet()) {
			if (!e.getKey().startsWith(folder+"/"))
				continue;
			
			if (e.getKey().indexOf('/',folder.length()+1) != -1) 
				continue; // In a subfolder of the given one.
			parsers.add(e.getValue());
		}
		return parsers;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection<XMLParser> getSubdocuments() {
		return (Collection)(subdocuments == null ? 
				Collections.emptyList() : 
				subdocuments.values());
	}

	public XMLParser getSubdocument(String relpath) {
		if (subdocuments == null)
			subdocuments = new HashMap<String, XMLParser>();
		relpath = relpath.replace(File.separatorChar, '/');
		if (!relpath.startsWith("/"))
			relpath = "/"+relpath;
		XMLParser p = subdocuments.get(relpath);
		if (p != null)
			return p;
		
		File f = new File(getUnzipLocationDocumentFile(),relpath.replace("/", File.separator));
		if (f.exists()) {
			p = new XMLParser(f);
			subdocuments.put(relpath, p);
		}
		return p;
	}   
    
	public String getBasePath() {
		 return getUnzipLocationDocumentFile().getAbsolutePath();
	}
	
	public String getRelativePath(XMLParser p) {
		String path = p.getXmlFile().getAbsolutePath();
		String basePath = getBasePath();
		if (!(path.startsWith(basePath)))
			return null;

		path = path.substring(basePath.length());
		path = path.replace(File.separatorChar, '/');
		return path;
	}

	private XMLParser contentTypes;
}
