package org.eclipse.gendoc.services.pptx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.xml.xpath.XPathExpressionException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gendoc.document.parser.documents.XMLParser;
import org.eclipse.gendoc.document.parser.documents.helper.XMLHelper;
import org.eclipse.gendoc.document.parser.pptx.Activator;
import org.eclipse.gendoc.document.parser.pptx.PPTXDocument;
import org.eclipse.gendoc.document.parser.pptx.PPTXHelper;
import org.eclipse.gendoc.document.parser.pptx.PPTXParser;
import org.eclipse.gendoc.document.parser.pptx.PPTXRef;
import org.eclipse.gendoc.document.parser.pptx.XPathPptxUtils;
import org.eclipse.gendoc.documents.IDocumentManager;
import org.eclipse.gendoc.documents.IDocumentService;
import org.eclipse.gendoc.process.AbstractProcess;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.exception.GenDocException;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PPTXUnflattenProcess extends AbstractProcess {

	@Override
	protected void doRun() throws GenDocException {
        final IDocumentManager documentManager = GendocServices.getDefault().getService(IDocumentManager.class);
        if (!(documentManager.getDocTemplate() instanceof PPTXDocument))
        	return;
        
        final IDocumentService documentService = GendocServices.getDefault().getService(IDocumentService.class);
        PPTXDocument document = (PPTXDocument)documentManager.getDocTemplate();
        document.jumpToStart();
        
        PPTXParser pptxParser = (PPTXParser)document.getXMLParser();
        org.w3c.dom.Document doc = pptxParser.getDocument();
		// Retrieve Slides
		
		List<Node> parasToRemove = null; 
		DocumentFragment activeSlide = null;
		XMLParser activeSlideParser = null;
		List<DocumentFragment> newSlides = new ArrayList<DocumentFragment>();
		List<DocumentFragment> newSlideRels = new ArrayList<DocumentFragment>();
		Node flatStruct = null;
		NodeList flatStructList = null;
		try {
			flatStruct = XPathPptxUtils.INSTANCE.evaluateNode(doc, "//"+PPTXHelper.GENDOC_FLAT_STRUCTURE_ELEM);
			if (flatStruct != null)
				flatStructList = XPathPptxUtils.INSTANCE.evaluateNodes(flatStruct, "./*");
		} catch (XPathExpressionException e) {
			Activator.log(e,IStatus.ERROR);
			return;
		}
		
		if (flatStruct == null || flatStructList == null)
			return;
		
		for (int i = 0; i<flatStructList.getLength(); i++) {
			Element el = (Element)flatStructList.item(i);
			if (el.getNodeName().equals(PPTXHelper.GENDOC_REF_ELEM)) {
				// Check Type:
				String typeStr = el.getAttribute(PPTXHelper.GENDOC_REF_ELEM_TYPE_ATTR);
				PPTXHelper.GENDOC_REF_TYPE type = PPTXHelper.GENDOC_REF_TYPE.valueOf(typeStr);

				String ref = el.getAttribute(PPTXHelper.GENDOC_REF_ELEM_REF_ATTR);
				PPTXRef slideRef = PPTXRef.parse(ref);
				switch (type) {
					// Start Slide: Clone the slide and select slide
					case startSlide:
						activeSlideParser = document.getSubdocument(slideRef.partPath);
						Element sldEl = (Element)activeSlideParser.getDocument().getDocumentElement().cloneNode(true);
						activeSlide = activeSlideParser.getDocument().createDocumentFragment();
						activeSlide.appendChild(sldEl);
						newSlides.add(activeSlide);
						
						String relsPath = slideRef.partPath.replace("/ppt/slides/", "/ppt/slides/_rels/") + ".rels";
						XMLParser relsP = document.getSubdocument(relsPath);
						DocumentFragment relsFrag = relsP.getDocument().createDocumentFragment();
						relsFrag.appendChild(relsP.getDocument().getDocumentElement().cloneNode(true));
						newSlideRels.add(relsFrag);						
						Set<String> parasRefs = new LinkedHashSet<String>( Arrays.asList(el.getAttribute("paras").split(",")));
						parasToRemove = new ArrayList<Node>();
						for (String refToRemove : parasRefs) {
							try {
								Element par = (Element)XPathPptxUtils.INSTANCE.evaluateNode(
										activeSlide,PPTXRef.parse(refToRemove).xpathSelector);
								if (par != null) {
									parasToRemove.add(par);
								}
							} catch (XPathExpressionException e) {}														
						}
						break;
					// End Slide: unselect slide
					case endSlide:
						for (Node n : parasToRemove) {
							if (n.getNodeName().equals("a:p")) {
								try {
									if (XPathPptxUtils.INSTANCE.evaluateNodes(n.getParentNode(),"./a:p").getLength() == 1) {
										n = XPathPptxUtils.INSTANCE.evaluateNode(n,"ancestor::p:sp");
									}
								} catch (XPathExpressionException e) {}
							}
							n.getParentNode().removeChild(n);
						}
						activeSlide = null;			
						activeSlideParser = null;
						break;
				}
			} else if (el.getNodeName().equals(PPTXHelper.DRAWING_ML_TEXT_PARAGRAPH )) {
				if (activeSlide == null)
					continue;
				try {
					// Check for a image.
					PPTXRef parRef = PPTXRef.parse(el.getAttribute(PPTXHelper.GENDOC_XPATHREF_ATTR));
					Element par = (Element)XPathPptxUtils.INSTANCE.evaluateNode(activeSlide,parRef.xpathSelector);
					if (par == null)
						continue;

					Node pic = XPathPptxUtils.INSTANCE.evaluateNode(el, ".//gendoc:pic");
					if (pic != null) {			            
						PPTXImageService imgService = (PPTXImageService)documentService.getAdditionalResourceService().getImageService();
						
						String imageId = XPathPptxUtils.INSTANCE.evaluateText(pic,"./@imageId");
						boolean keepH = "true".equals(XPathPptxUtils.INSTANCE.evaluateText(pic,"./@keepH"));
						boolean keepW = "true".equals(XPathPptxUtils.INSTANCE.evaluateText(pic,"./@keepW"));
						boolean maxH = "true".equals(XPathPptxUtils.INSTANCE.evaluateText(pic,"./@maxH"));
						boolean maxW = "true".equals(XPathPptxUtils.INSTANCE.evaluateText(pic,"./@maxW"));						
						imgService.replaceParagraphWithPicture(document, activeSlideParser, par, imageId, keepH, keepW, maxH, maxW);
						Element sp = (Element)XPathPptxUtils.INSTANCE.evaluateNode(par,"ancestor::p:sp");
						sp.getParentNode().removeChild(sp);						
						parasToRemove.remove(par);
					} else {				
						el.removeAttribute(PPTXHelper.GENDOC_XPATHREF_ATTR);
						el.removeAttribute("xmlns:gendoc");
						par.getParentNode().insertBefore(par.getOwnerDocument().importNode(el,true), par);
						par.getParentNode().removeChild(par);
						parasToRemove.remove(par);
					}
				} catch (XPathExpressionException e) {}					
			}
		};
		
		try {
			Node slideList = XPathPptxUtils.INSTANCE.evaluateNode(pptxParser.getDocument(),"//"+PPTXHelper.SLIDE_ID_LIST_NODE);
			NodeList nl = slideList.getChildNodes();
			while (nl.getLength()>0)
				slideList.removeChild(nl.item(0));
			
			int index = 0;
			for (DocumentFragment slide : newSlides) {
				try {
					document.createSubdocument("/ppt/slides/slide"+(index+1)+".xml", 
							XMLHelper.asText(slide.getFirstChild(), true),
							PPTXHelper.SLIDE_CONTENT_TYPE);
					document.createSubdocument("/ppt/slides/_rels/slide"+(index+1)+".xml.rels", 
							XMLHelper.asText(newSlideRels.get(index).getFirstChild(), true));
					String relId = pptxParser.addRelationship(PPTXHelper.SLIDE_RELATIONSHIP, "/ppt/slides/slide"+(index+1)+".xml");
					
					Element sldIdEl = slideList.getOwnerDocument().createElement(PPTXHelper.SLIDE_ID_NODE);
					sldIdEl.setAttribute(PPTXHelper.SLIDE_ID_ATTR, relId);
					sldIdEl.setAttribute("id", ""+(256+index));
					slideList.appendChild(sldIdEl);
				} catch (IOException e) {
					Activator.log(e, IStatus.ERROR);
				}			
				index ++;
			}
		} catch (XPathExpressionException e) {}
		
		flatStruct.getParentNode().removeChild(flatStruct);
	}		

	@Override
	protected int getTotalWork() {
		// TODO Auto-generated method stub
		return 1;
	}

}
