/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.document.parser.pptx;

import org.eclipse.gendoc.document.parser.documents.openoffice.OpenOfficeXPathUtils;

public class XPathPptxUtils extends OpenOfficeXPathUtils {
	public static final XPathPptxUtils INSTANCE = new XPathPptxUtils();
	
	private XPathPptxUtils() {
		super(new PPTXNamespaceContext());
	}

}
