/*****************************************************************************
 * Copyright (c) 2018 Ericsson AB.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.bundle.acceleo.html.files;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class HtmlStripTagVisitor implements IHtmlVisitor {
	public HtmlStripTagVisitor() {
		buf = new StringBuilder();
	}
	
	public String toString() {
		return buf.toString();
	}
	
	@Override
	public void visitText(Text node) {
		buf.append(node.getNodeValue());
	}
	
	@Override
	public void visitElementAfter(Element node) {
		String tag = node.getTagName();
		if (tag.equals("p")) {
			newLine();
		} else if (tag.equals("li")) {
			if (!isStartingLine())
				newLine();
		}				
	}
	
	@Override
	public boolean visitElement(Element node) {
		String tag = node.getTagName();
		if (tag.equals("p")) {
			if (!isStartingLine())
				newLine();
		} else if (tag.equals("br")) {
			newLine();
			return false;
		} 
		
		if (tag.equals("ol") || tag.equals("ul")) {
			if (!isStartingLine())
				newLine();
		}
		return true;
	}
	
	@Override
	public void visitDocumentAfter(Document node) {}
	
	@Override
	public boolean visitDocument(Document node) {
		return true;
	}
	
	protected void newLine() {
		buf.append("\n");
	}
	
	protected boolean isStartingLine() {
		int pos = buf.length();
		if (pos == 0)
			return true;
		pos--;
		while (pos >= 0) {
			char ch = buf.charAt(pos); 
			if (ch == '\n' || ch == Character.LINE_SEPARATOR || ch == Character.PARAGRAPH_SEPARATOR )
				return true;
			if (!Character.isWhitespace(ch))
				return false;
			pos--;
		}
		return true;
	}

	protected StringBuilder buf;
}
